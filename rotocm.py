# program: ROTOCM

# Developer: I. Kolotov
# Modifications: S. Streltsov

#-------------------------------------
#- reads occupation matrix from OUTCAR
#- and rotates it to the local coordinate
#- system, defined by Euler angels 
#- alpha, beta, gamma, which can be
#- obtained using NNEIGH6 utility
#- for the octahedra
#-------------------------------------

# Restrictions: works only in collinear case

#!pip install pymatgen
# read POSCAR
from pymatgen.io.vasp import Poscar
import numpy as np
import array
import sys
import math
#from google.colab import drive


def mk_rlmc(l,alpha,beta,gamma):

  fa = np.zeros(21)
  for i in range(21):
    fa[i] = math.factorial(i)

  a1 = np.zeros((2*l+1, 2*l+1), 'complex')
  rlm = np.zeros((2*l+1, 2*l+1), 'complex')
  cobeta = np.cos(beta/2.)
  sibeta = np.sin(beta/2.)
  ecom = complex(math.e, 0)
      
  for m in range(-l, l+1):
    for q in range(-l, l+1):
      h_min = max(0, q-m)
      h_max = min(l-m, l+q)
      sum = 0.
      sql = math.sqrt(fa[l+m]*fa[l-m]*fa[l+q]*fa[l-q])
      for h in range(h_min, h_max + 1):
        tmp1 = (fa[l-m-h]*fa[l+q-h]*fa[h-q+m]*fa[h])
        tmp2 = cobeta**(2*l-m+q-2*h)*sibeta**(m-q+2*h)
        sum = sum + (-1)**h*(tmp2*sql/tmp1)
      rlm[m+l, q+l] = ecom**(complex(0., -(q*alpha + m*gamma)))*sum

  #print(rlm)
  #print("\n")
  
  s = 1./math.sqrt(2.)
  if (l == 0):
    a1[0][0] = complex(1., 0.)
  elif (l == 1):
    a1[0][0] = complex(0., s)
    a1[0][2] = complex(0., s)
    a1[1][1] = complex(1., 0.)
    a1[2][0] = complex(s, 0.)
    a1[2][2] = complex(-s, 0.)
  elif (l == 2):
    a1[0][0] = complex(0., s)
    a1[0][4] = complex(0., -s)
    a1[1][1] = complex(0., s)
    a1[1][3] = complex(0., s)
    a1[2][2] = complex(1., 0.)
    a1[3][1] = complex(s, 0.)
    a1[3][3] = complex(-s, 0.)
    a1[4][0] = complex(s, 0.)
    a1[4][4] = complex(s, 0.)
  else:
    a1[0][0] = complex(0., s)
    a1[0][6] = complex(0., s)
    a1[1][1] = complex(0., s)
    a1[1][5] = complex(0., -s)
    a1[2][2] = complex(0., s)
    a1[2][4] = complex(0., s)
    a1[3][3] = complex(1., 0.)
    a1[4][2] = complex(s, 0.)
    a1[4][4] = complex(-s, 0.)
    a1[5][1] = complex(s, 0.)
    a1[5][5] = complex(s, 0.)
    a1[6][0] = complex(s, 0.)
    a1[6][6] = complex(-s, 0.)

  #print(a1)
  #print("\n")

  for lam in range(0, 2*l+1):
    for m in range(0, 2*l+1):
      sumc = complex(0., 0.)
      for nu in range(0, 2*l+1):
        for mu in range(0, 2*l+1):
          sumc = sumc + a1[lam][nu] * rlm[nu][mu] * (a1[m][mu]).conjugate()
      rlmc[lam][m] = sumc

  return rlmc

print("\n")
print("       .............................................")
print("       ...  program for density matrix rotation  ...")
print("       .............................................")
print("\n")

outcar = open('OUTCAR','r')

found_atom = False
poscar = Poscar.from_file('POSCAR')
natoms = poscar.natoms
site_symbols = poscar.site_symbols
numclasses = len(poscar.natoms)

while True:
  line = outcar.readline()

  #check whether we at the end of file or nor 
  if 'Total CPU time used' in line: break

  #define for how many (which) atoms U correction is applied
  if 'LDAUL' in line:
    umask = (line).split('=')[1].split()
    for i in range(len(umask)):
      umask[i] = int(umask[i])
    print('Information from POSCAR:\n')
    print('Atoms: ', site_symbols)
    print('Classes: ', natoms)
    print('U-mask:  ', umask)
    numatoms = 0
    u_index = []
    occ_up = []
    occ_dn = []
    for i in range(len(umask)):
      if (umask[i] == 2):
        u_index.append(i)
        numatoms += natoms[i]
    for i in range(numatoms):
      occ_up.append([])
      occ_dn.append([])
    print('Number of atoms with occupation matrix: ' + str(numatoms))
    lgth = len(u_index)      
    
  #read occupation matrixes for a given iteration
  #we do this in cycle, so that all iteration are read
  if 'SETDIJ' in line:
    #l = np.zeros(numatoms)
    ll = []
    atom = []
    for iatom in range(numatoms):
      line = outcar.readline()
      occ_up_atom = []
      occ_dn_atom = []
      atom.append(int(line.split('=')[1].split('type')[0].strip()))
      #l[iatom] = int(line.split('=')[3].strip())
      ll.append(int(line.split('=')[3].strip()))
      for i in range(5): line = outcar.readline()  #lines we don't need
      for i in range(2*ll[iatom]+1):
        occ_up_atom.append([float(item) for item in outcar.readline().split()])
      for i in range(3): line = outcar.readline() #lines we don't need
      for i in range(2*ll[iatom]+1):
        occ_dn_atom.append([float(item) for item in outcar.readline().split()])
        #print('atom:' + str(atom) + '\n  spin 1:' + str(occ_up_atom) + '\n  spin 2:' + str(occ_dn_atom))
      for i in range(13): line = outcar.readline() #lines we don't need
        #print(occ_up_atom)
      occ_up[iatom] = occ_up_atom
      occ_dn[iatom] = occ_dn_atom

shft = np.zeros(lgth, 'int')
shft[0] = 0
for i in range(lgth-1):
  shft[i+1] = shft[i] + natoms[u_index[i]]

l = np.zeros(lgth, 'int')
for i in range(lgth):
  l[i] = ll[i + shft[i]] 

outcar.close()
#poscar.close()

#if (l == 0): 
#  orb = 's'
#elif (l == 1): 
#  orb = 'p'
#elif (l == 2): 
#  orb = 'd'
#else: 
#  orb = 'f'

pi = 4. * np.arctan(1.)

Fout = open( "rotocm.out", "w" )
print('\n')
print(' Would you prefer to enter the Euler angles in')
print('       pi-units (pi) or radians (rad) ?')
print('               (default is pi) ')
angch = input().split()
angch = angch[0]
print('\n')

for clas in range(lgth):
  for cout in range(natoms[u_index[clas]]):

    if (angch == 'rad'):

      print('input Euler angles (alpha beta gamma) for', site_symbols[u_index[clas]],'atom №', cout+1,' (or type Stop for exit):')
      angles = input().split() 
      if angles[0] == 'Stop':
        sys.exit("Done!")
      alpha = float(angles[0])
      beta = float(angles[1])
      gamma = float(angles[2])

      print('alpha (pi-units):', alpha*pi, '|' , 'alpha (radians):', alpha, file=Fout)
      print('beta (pi-units):', beta*pi, '|' , 'beta (radians):', beta, file=Fout)
      print('gamma (pi-units):', gamma*pi, '|' , 'gamma (radians):', gamma, file=Fout)
    else:
      print('input Euler angles (alpha beta gamma) for', site_symbols[u_index[clas]],'atom №', cout+1,' (or type Stop for exit):')
      angles = input().split()
      if angles[0] == 'Stop':
        sys.exit("Done!")
      alpha = float(angles[0])*pi
      beta = float(angles[1])*pi
      gamma = float(angles[2])*pi

      print('alpha (pi-units):', alpha, '|' , 'alpha (radians):', alpha/pi, file=Fout)
      print('beta (pi-units):', beta, '|' , 'beta (radians):', beta/pi, file=Fout)
      print('gamma (pi-units):', gamma, '|' , 'gamma (radians):', gamma/pi, file=Fout)
 
    lm = 2*l[clas] + 1
    #rlmc = np.zeros((2*l[0]+1,2*l[0]+1), 'complex')
    rlmc = np.zeros((lm, lm))
    rlmc = mk_rlmc(l[clas],alpha,beta,gamma)

    dens_up = np.zeros((lm, lm))
    dens_dn = np.zeros((lm, lm))
    dens_up_dn = np.zeros((lm, lm))
    dens_up_new = np.zeros((lm, lm))
    dens_dn_new = np.zeros((lm, lm))
    dens_up_dn_new = np.zeros((lm, lm))

    for i in range(lm):
      for j in range(lm):
        dens_up[i][j] = occ_up[cout + shft[clas]][i][j]
        dens_dn[i][j] = occ_dn[cout + shft[clas]][i][j]
 
    dens_up_dn = dens_up - dens_dn

    for m1 in range(lm):
      for m2 in range(lm): 
        sum1 = 0.
        sum2 = 0.
        sum3 = 0. 
        for m3 in range(lm):
          for m4 in range(lm):
            sum1 = sum1 + rlmc[m1][m3]*dens_up[m3][m4]*rlmc[m2][m4]
            sum2 = sum2 + rlmc[m1][m3]*dens_dn[m3][m4]*rlmc[m2][m4]
            sum3 = sum3 + rlmc[m1][m3]*dens_up_dn[m3][m4]*rlmc[m2][m4]
        dens_up_new[m1][m2] = sum1
        dens_dn_new[m1][m2] = sum2
        dens_up_dn_new[m1][m2] = sum3 

    print('\n')
    print('\n', file=Fout)
    print('For', site_symbols[u_index[clas]],'atom №', cout+1)   # '(atom', atom[cout + shft[clas]], ')'
    print('For', site_symbols[u_index[clas]],'atom №', cout+1, file=Fout)
    print('\n')
    print('\n', file=Fout)

    for i in range(lm):
      for j in range(lm):
        #dens_up_new[iatom][i][j] = round(dens_up_new[iatom][i][j], 6)
        #dens_dn_new[iatom][i][j] = round(dens_dn_new[iatom][i][j], 6)
        #dens_up_dn_new[iatom][i][j] = round(dens_up_dn_new[iatom][i][j], 6)
        dens_up_new[i][j] = format(dens_up_new[i][j], "11.6f")
        dens_dn_new[i][j] = format(dens_dn_new[i][j], "11.6f")
        dens_up_dn_new[i][j] = format(dens_up_dn_new[i][j], "11.6f")

    print('Rotation matrix:')
    print('Rotation matrix:', file=Fout)
    print(rlmc)
    print(rlmc, file=Fout)
    print('\n')
    print('\n', file=Fout)

    print('Majority spin, initial occupancy matrix:')
    print('Majority spin, initial occupancy matrix:', file=Fout)
    print(dens_up)
    print(dens_up, file=Fout)
    print('\n')
    print('\n', file=Fout)

    print('Minority spin, initial occupancy matrix:')
    print('Minority spin, initial occupancy matrix:', file=Fout)
    print(dens_dn)
    print(dens_dn, file=Fout)
    print('\n')
    print('\n', file=Fout)

    print('Majority-minority spin, initial occupancy matrix:')
    print('Majority-minority spin, initial occupancy matrix:', file=Fout)
    print(dens_up_dn)
    print(dens_up_dn, file=Fout)
    print('\n')
    print('\n', file=Fout)

    print('Majority spin, rotated occupancy matrix:')
    print('Majority spin, rotated occupancy matrix:', file=Fout)
    print(dens_up_new)
    print(dens_up_new, file=Fout)
    print('\n')
    print('\n', file=Fout)

    print('Minority spin, rotated occupancy matrix:')
    print('Minority spin, rotated occupancy matrix:', file=Fout)
    print(dens_dn_new)
    print(dens_dn_new, file=Fout)
    print('\n')
    print('\n', file=Fout)

    print('Majority-minority spin, rotated occupancy matrix:')
    print('Majority-minority spin, rotated occupancy matrix:', file=Fout)
    print(dens_up_dn_new)
    print(dens_up_dn_new, file=Fout)
    print('\n')
    print('\n', file=Fout)

    print('...................................................................')
    print('...................................................................', file=Fout)

Fout.close()

