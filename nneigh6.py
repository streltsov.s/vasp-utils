# program: NNEIGH6

# Developers: I. Kolotov, S. Streltsov

#-------------------------------------
#- reads POSCAR and finds for octahedra
#- Euler angles, which transform axes
#- to point to ligands.
#
# NEW: prints out matrix for transformation
#      d-orbitals
#
#-------------------------------------

# Restrictions: works only for octahedra


#!pip install pymatgen

# read POSCAR
from pymatgen.io.vasp import Poscar
import numpy as np
import array
import sys
import math

def mk_rlmc(l,alpha,beta,gamma):

  fa = np.zeros(21)
  for i in range(21):
    fa[i] = math.factorial(i)

  a1 = np.zeros((2*l+1, 2*l+1), 'complex')
  rlm = np.zeros((2*l+1, 2*l+1), 'complex')
  cobeta = np.cos(beta/2.)
  sibeta = np.sin(beta/2.)
  ecom = complex(math.e, 0)
      
  for m in range(-l, l+1):
    for q in range(-l, l+1):
      h_min = max(0, q-m)
      h_max = min(l-m, l+q)
      sum = 0.
      sql = math.sqrt(fa[l+m]*fa[l-m]*fa[l+q]*fa[l-q])
      for h in range(h_min, h_max + 1):
        tmp1 = (fa[l-m-h]*fa[l+q-h]*fa[h-q+m]*fa[h])
        tmp2 = cobeta**(2*l-m+q-2*h)*sibeta**(m-q+2*h)
        sum = sum + (-1)**h*(tmp2*sql/tmp1)
      rlm[m+l, q+l] = ecom**(complex(0., -(q*alpha + m*gamma)))*sum

  #print(rlm)
  #print("\n")
  
  s = 1./math.sqrt(2.)
  if (l == 0):
    a1[0][0] = complex(1., 0.)
  elif (l == 1):
    a1[0][0] = complex(0., s)
    a1[0][2] = complex(0., s)
    a1[1][1] = complex(1., 0.)
    a1[2][0] = complex(s, 0.)
    a1[2][2] = complex(-s, 0.)
  elif (l == 2):
    a1[0][0] = complex(0., s)
    a1[0][4] = complex(0., -s)
    a1[1][1] = complex(0., s)
    a1[1][3] = complex(0., s)
    a1[2][2] = complex(1., 0.)
    a1[3][1] = complex(s, 0.)
    a1[3][3] = complex(-s, 0.)
    a1[4][0] = complex(s, 0.)
    a1[4][4] = complex(s, 0.)
  else:
    a1[0][0] = complex(0., s)
    a1[0][6] = complex(0., s)
    a1[1][1] = complex(0., s)
    a1[1][5] = complex(0., -s)
    a1[2][2] = complex(0., s)
    a1[2][4] = complex(0., s)
    a1[3][3] = complex(1., 0.)
    a1[4][2] = complex(s, 0.)
    a1[4][4] = complex(-s, 0.)
    a1[5][1] = complex(s, 0.)
    a1[5][5] = complex(s, 0.)
    a1[6][0] = complex(s, 0.)
    a1[6][6] = complex(-s, 0.)

  for lam in range(0, 2*l+1):
    for m in range(0, 2*l+1):
      sumc = complex(0., 0.)
      for nu in range(0, 2*l+1):
        for mu in range(0, 2*l+1):
          sumc = sumc + a1[lam][nu] * rlm[nu][mu] * (a1[m][mu]).conjugate()
      drot[lam][m] = sumc

  return drot

def block_8(i, nn , nm , d):
  tol = 1.e-6
  i=i+1
  flag = False
  for j in range(i+1,6):
    if (abs(d[j] - d[i]) < tol and abs(nm[i][0] + nm[j][0]) < tol 
        and abs(nm[i][1] + nm[j][1]) < tol and abs(nm[i][2] + nm[j][2]) < tol):
      for k in range(3):
        dm = nn[j][k]
        nn[j][k] = nn[i+1][k]
        nn[i+1][k] = dm
        dm = nm[j][k]
        nm[j][k] = nm[i+1][k]
        nm[i+1][k] = dm
      #npa = tnO[j]
      #tnO[j] = tnO[i+1]
      #tnO[i+1] = npa
      for m in range(3):
        if (abs(nm[i][m]) > tol):
          if (nm[i][m] > nm[i+1][m]):
            i = i+1
            flag = True
          else:
            for k in range(3):
              dm = nn[i][k]
              nn[i][k] = nn[i+1][k]
              nn[i+1][k] = dm
              dm = nm[i][k]
              nm[i][k] = nm[i+1][k]
              nm[i+1][k] = dm
          #npa = tnO[i]
          #tnO[i] = tnO[i+1]
          #tnO[i+1] = npa
            i=i+1
            flag = True
        if flag:
          break
      if (flag == True):
        pass
      else :
        print("wrong coordinates of p-atoms")
        sys.exit()
    if flag:
      break            
  if (i <= 4): 
    return block_8(i, nn , nm , d)
  else:
    return i , nn , nm

def input_par():
  print(" ")
  print("First (f) middle (m) or last (l) two", anionname 
        ,"atoms will site about new z-direction ?")
  choise = input().split()
  if (choise[0] == "f" or choise[0] == "F"):
    #print(" ")
    #print("First two", anionname ,"atoms will site about new z-direction", file = Fout)
    i0 = 3
    i1 = 4
    i2 = 6
    j1 = 1
    j2 = 2
    return i0, i1, i2, j1, j2
  elif (choise[0] == "m" or choise[0] == "M"):
    #print(" ")
    #print("Middle two", anionname ,"atoms will site about new z-direction". file = Fout)
    i0 = 1
    i1 = 5
    i2 = 6
    j1 = 3
    j2 = 4
    return i0, i1, i2, j1, j2
  elif (choise[0] == "l" or choise[0] == "L"):
    #print(" ")
    #print("Last two", anionname ,"atoms will site about new z-direction", file = Fout)
    i0 = 1
    i1 = 2
    i2 = 4
    j1 = 5
    j2 = 6
    return i0, i1, i2, j1, j2
  else:
    input_par()

def chk(z):
  chk = False
  if (abs(z) < 5.e-10): # -13
    chk = True
  return chk

def che(z):
  che = False
  if (abs(z) < 5.e-5): # -8
    che = True
  return che

def dc2matr(b,d,c):

  dc = np.cos(d)
  ds = np.sin(d)
  for i in range(3):
    for j in range(3):
      b[j][i] = (1. - dc)*c[i]*c[j]
  for i in range(3):
    b[i][i] = b[i][i] + dc

  b[1][0] = b[1][0] + c[2]*ds
  b[2][0] = b[2][0] - c[1]*ds
  b[0][1] = b[0][1] - c[2]*ds
  b[2][1] = b[2][1] + c[0]*ds
  b[0][2] = b[0][2] + c[1]*ds
  b[1][2] = b[1][2] - c[0]*ds
  return b


def rotnb(a,old1,old2,old3,new1,new2,new3):
  old = np.zeros(3)
  old = [old1 , old2 , old3]
  new = np.zeros(3)
  new = [new1 , new1 , new1]
  for i in range(3):
    new[i] = 0.
    for j in range(3):
      new[i] = new[i] + a[j][i]*old[j]
  return new[0] , new[1] , new[2]



def matr2ang(a,alpha,beta,gamma):
  pi = 4. * np.arctan(1.)
  dpi = 2. * pi
  ok = True
  
  z = a[2][2]
  if (abs(z) > 1.):
    z = 1. * z/abs(z)
  beta = np.arccos(z)

  if (chk(a[2][2] - 1.) or chk(a[2][2] + 1.)):
    z = a[0][0]*a[2][2]
    if (abs(z) > 1.):
      z = 1. * z/abs(z)
    alpha = np.arccos(z)
    gamma = 0.
  else:
    t = 1./np.sin(beta)
    z = -a[0][2] * t
    if (abs(z) > 1.):
      z = 1. * z/abs(z)
    alpha = np.arccos(z)
    z = a[2][0] * t
    if (abs(z) > 1.):
      z = 1. * z/abs(z)
    gamma = np.arccos(z)

  ok = chkang(a,alpha,beta,gamma)
  if (ok == True):
    pass
  else:
    alpha = - alpha # to new
    ok = chkang(a,alpha,beta,gamma)
    if (ok == True):
      pass
    else:
      alpha = - alpha # to ini
      beta  = - beta # to new
      ok = chkang(a,alpha,beta,gamma)
      if (ok == True):
        pass
      else:
        beta  = - beta # to ini
        gamma = - gamma # to new
        ok = chkang(a,alpha,beta,gamma)
        if (ok == True):
          pass
        else:
          gamma = - gamma # to ini
          alpha = - alpha # to new
          beta  = - beta # to new
          ok = chkang(a,alpha,beta,gamma)
          if (ok == True):
            pass
          else:
            alpha = - alpha # to ini
            beta  = - beta # to ini
            alpha = - alpha # to new
            gamma = - gamma # to new
            ok = chkang(a,alpha,beta,gamma)
            if (ok == True):
              pass
            else:
              alpha = - alpha # to ini
              gamma = - gamma # to ini
              beta  = - beta # to new
              gamma = - gamma # to new
              ok = chkang(a,alpha,beta,gamma)
              if (ok == True):
                pass
              else:
                beta  = - beta # to ini
                gamma = - gamma # to ini
                alpha = - alpha
                beta  = - beta
                gamma = - gamma
                ok = chkang(a,alpha,beta,gamma)
                if (ok == True):
                  pass
                else:
                  print("error in Euler angles finding")
                  sys.exit()
  return alpha , beta , gamma

def chkang(r,a,b,g):
  t = np.zeros((3,3))
  t = euler(a,b,g,t)
  
  ok = True
  for i in range(3):
    for j in range(3):
      if ( not che( t[j][i] - r[j][i] ) ):
        ok = False
  return ok


def euler(a,b,g,t):

  t[0][0] = np.cos(a)*np.cos(b)*np.cos(g) - np.sin(a)*np.sin(g)
  t[0][1] = np.cos(a)*np.cos(b)*np.sin(g) + np.sin(a)*np.cos(g)
  t[0][2] =-np.cos(a)*np.sin(b)

  t[1][0] =-np.sin(a)*np.cos(b)*np.cos(g) - np.cos(a)*np.sin(g)
  t[1][1] =-np.sin(a)*np.cos(b)*np.sin(g) + np.cos(a)*np.cos(g)
  t[1][2] = np.sin(a)*np.sin(b)

  t[2][0] = np.sin(b)*np.cos(g)
  t[2][1] = np.sin(b)*np.sin(g)
  t[2][2] = np.cos(b)

  return t

def rse(A,B,C,E,F,G,D,r11,r12,r13,r21,r22,r23):
  r1 = np.zeros(3)
  r1 = [r11 , r12 , r13]
  r2 = np.zeros(3)
  r2 = [r21 , r22 , r23]  


# to solve the system of equations:
#     Ax + By + Cz = 0
#     Ex + Fy + Gz = D
#     x**2 + y**2 + z**2 = 1
# roots are kept as r1 & r2 vectors

  if ( chk(B) and chk(C)):
    if (chk(F)):
      if ( chk( (D/G)**2 - 1. ) ):
        r1[0] = 0.
        r1[1] = 0.
        r1[2] = D/G
        r2[0] = 0.
        r2[1] = 0.
        r2[2] = D/G
        return r1[0] , r1[1] , r1[2] , r2[0] , r2[1] , r2[2]
      else:
        print("F=0 in rse (B=C=0)")
        sys.exit()
    s = G*G + F*F
    if (chk(s)):
      print("G*G+F*F=0 in rse")
      sys.exit()
    O = F*F + G*G - D*D
    if (chk(O)):
      O = 0.
    if (O < 0):
      print("F*F+G*G-D*D < 0 in rse")
      sys.exit()
    O = math.sqrt(O)
    r1[2] = (D*G + F*O)/s
    r1[1] = -(G*r1[2]-D)/F
    r1[0] = 0.
    r2[2] = (D*G-F*O)/s
    r2[1] = -(G*r2[2]-D)/F
    r2[0] = 0.
    return r1[0] , r1[1] , r1[2] , r2[0] , r2[1] , r2[2]

  if (chk(A) and chk(C)):
    if (chk(G)):
      if ( chk( (D/E)**2 - 1. ) ):
        r1[0] = D/E
        r1[1] = 0.
        r1[2] = 0.
        r2[0] = D/E
        r2[1] = 0.
        r2[2] = 0.
        return r1[0] , r1[1] , r1[2] , r2[0] , r2[1] , r2[2]
      else:
        print("G=0 in rse (A=C=0)")
        sys.exit
    s = G*G + E*E
    if (chk(s)): 
      print("G*G+E*E=0 in rse")
      sys.exit()
    if (chk(O)):
      O = 0.
    O = E*E + G*G - D*D
    if (O < 0):
      print("E*E+G*G-D*D < 0 in rse")
      sys.exit()
    O = math.sqrt(O)
    r1[0] = (D*E + G*O)/s
    r1[1] = 0.
    r1[2] = -(E*r1[0] - D)/G
    r2[0] = (D*E - G*O)/s
    r2[1] = 0.
    r2[2] = -(E*r2[0] - D)/G
    return r1[0] , r1[1] , r1[2] , r2[0] , r2[1] , r2[2]

  if (chk(A) and chk(B)):
    if (chk(F)):
      if ( chk( (D/E)**2 - 1. ) ):
        r1[0]=D/E
        r1[1] = 0.
        r1[2] = 0.
        r2[0] = D/E
        r2[1] = 0.
        r2[2] = 0.
        return r1[0] , r1[1] , r1[2] , r2[0] , r2[1] , r2[2]
      else:
        print("F=0 in rse (A=B=0)")
        sys.exit()
    s = F*F + E*E
    if (chk(s)):
      print("F*F+E*E=0 in rse")
      sys.exit()
    if (chk(O)):
      O = 0.
    O = E*E + F*F - D*D
    if (O < 0):
      print("E*E+F*F-D*D < 0 in rse")
      sys.exit()
    O = math.sqrt(O)
    r1[0] = (D*E + F*O)/s
    r1[1] = -(E*r1[0] - D)/F
    r1[2] = 0.
    r2[0] = (D*E - F*O)/s
    r2[1] = -(E*r2[0] - D)/F
    r2[2] = 0.
    return r1[0] , r1[1] , r1[2] , r2[0] , r2[1] , r2[2]

  s = A*F - B*E
  t = A*G - C*E
  u = B*G - C*F
  v = B*F + A*E
  w = A*A + B*B
  P = s**2 + t**2 + u**2
  Q = D*(C*v - G*w)
  R = D*D*w - s*s
  if (chk(P)):
    print("P=0 in rse")
    sys.exit()
  if (chk(s)):
    print("s=0 in rse")
    sys.exit()
  O = Q*Q - P*R
  if (chk(O)):
    O = 0.
  if (O < 0.):
    print("Q*Q-P*R<0 in rse")
    sys.exit()
  O = math.sqrt(O)
  r1[2] = (O-Q)/P
  r1[1] = -( t*r1[2] - A*D)/s
  r1[0] = -(-u*r1[2] + B*D)/s
  r2[2] = -(O + Q)/P
  r2[1] = -( t*r2[2] - A*D)/s
  r2[0] = -(-u*r2[2] + B*D)/s
  return r1[0] , r1[1] , r1[2] , r2[0] , r2[1] , r2[2]

poscar = Poscar.from_file('POSCAR')
lattice = poscar.structure.lattice.matrix
natoms = poscar.natoms
site_symbols = poscar.site_symbols
numclasses = len(poscar.natoms)
coords = poscar.structure.frac_coords

pi = 4. * np.arctan(1.)
numo=100

Fout = open( "nneigh6.out", "w" )
print("\n")
print("         ............................................")
print("         ....  program for Euler angles finding  ....")
print("         ............................................")
print("\n")
print("   Input the name of a central d atoms")
ndainp = input().split() 
ndainp = ndainp[0]
print(" ")
print("   Input the name of p-atoms which form octahedron around ", ndainp)
anionname = input().split()
anionname = anionname[0]
print(" ")
      
if ndainp in site_symbols:
  indx = [site_symbols.index(ndainp)] # Индекс атома в массиве имен (indx[0])
else:
  print("   there are no d-atoms named as", ndainp ,"in list. Try again!")
  sys.exit()

if (natoms[indx[0]] == 1):
  print("   There is one d-atom named as", ndainp ,"in list")
else:
  print("   There are", natoms[indx[0]] ,"d-atoms named as", ndainp ,"in list")


if anionname in site_symbols:
  indx.append(site_symbols.index(anionname)) #Индекс атома в массиве имен
else:
  print("   There are no d-atoms named as", anionname ,"in list. Try again!")
  sys.exit()

if (natoms[indx[1]] > numo):
  print("   It was found", natoms[indx[1]] ,"", anionname ,
        "atoms in list \n Increase the maximum allowable amount of p-atoms in the program!")
  sys.exit()

xp = [0 , 0] # кол-во атомов до d-атомов и анионов соответственно
for i in range(indx[0]):
  xp[0] = xp[0] + natoms[i]
for i in range(indx[1]):
  xp[1] = xp[1] + natoms[i]

pO = np.zeros((natoms[indx[1]], 3))
for i in range(natoms[indx[1]]):
  for j in range(3):
    pO[i][j] = coords[i + xp[1]][0]*lattice[0][j] + coords[i + xp[1]][1]*lattice[1][j] + coords[i + xp[1]][2]*lattice[2][j]

pMn = np.zeros(3)


#Main loop over all TM ions
for kknt in range(natoms[indx[0]]):

  #coordinates of given TM
  for i in range(3):
    pMn[i] = coords[xp[0] + kknt][0]*lattice[0][i] + coords[xp[0] + kknt][1]*lattice[1][i] + coords[xp[0] + kknt][2]*lattice[2][i]

  t = np.zeros((natoms[indx[1]]*125, 3))

  #coordinates of ligands
  num=-1
  for i in range(natoms[indx[1]]):
    for j in range(-2,3):
      for k in range(-2,3):
        for l in range(-2,3):
          num = num+1
          for m in range(3):
            t[num][m]=pO[i][m]+j*lattice[0][m]+k*lattice[1][m]+l*lattice[2][m] 
        
  # TM-O distances
  d = np.zeros(natoms[indx[1]]*125)
  for i in range(natoms[indx[1]]*125):
    d[i] = math.sqrt((t[i][0] - pMn[0])**2 + (t[i][1] - pMn[1])**2 + (t[i][2] - pMn[2])**2)

  nn = np.zeros((6, 3))
  for i in range(6):
    dm = d[i]
    num = i
    for j in range(i+1,natoms[indx[1]]*125):
      if (d[j] < dm and d[j] > 0.0001):
        num = j
        dm = d[j]
    if (num != i):
      d[num] = d[i]
      d[i] = dm
	    #tnO[num] = tnO[i]
	    #tnO[i] = npa
      for k in range(3):
        nn[i][k] = t[num][k]

  #coordinates with respect to TM
  nm = np.zeros((6, 3))
  for i in range(6):
    for j in range(3):
      nm[i][j] = nn[i][j] - pMn[j]

  # Case when some distances are equal:
  i = -1
  i , nn , nm = block_8(i, nn , nm , d)

  # alat = 10.2324931 , 0.529177  *10.2324931*0.529177 (0,52917720859363636)

  #print out information on distances
  print("         ............................................")
  print("\n")
  print("atom", ndainp,"in position : {0:8.4f} {1:8.4f} {2:8.4f}".format(pMn[0] , pMn[1] , pMn[2]))
  print("(in cartesian coordinates)")
  print("We move it to: {0:8.4f} {1:8.4f} {2:8.4f}".format(0, 0, 0))
  print("\n")
  print("Six nearest", anionname ,"atoms :            ||    these",
        anionname , "became:   ||  distance(Angs):")
  print("(in cartesian coordinates)")
  for i in range(6):
    print(i+1,". coords: {0:6.3f} {1:6.3f} {2:6.3f} || {3:6.3f} {4:6.3f} {5:6.3f} || {6:6.3f}".format(nn[i][0], nn[i][1], nn[i][2], nm[i][0], nm[i][1], nm[i][2], math.sqrt(nm[i][0]**2+nm[i][1]**2 + nm[i][2]**2))) 
  print("\n")

  print("\n", file=Fout)
  print("atom" , ndainp , "in position: %6.3f  %6.3f  %6.3f" % (pMn[0], pMn[1], pMn[2]), file=Fout)
  print("(in cartesian coordinates)", file=Fout)
  print("when it is moved to: %6.3f  %6.3f  %6.3f" % (0.e-4, 0.e-4, 0.e-4), file=Fout)
  print("\n", file=Fout)
  print("Six nearest", anionname ,"atoms :            ||     these",
        anionname , "became:   ||  distance(Angs):", file=Fout)
  print("(in cartesian coordinates)", file=Fout)
  for i in range(6):
    print(i+1,". coords: %6.3f  %6.3f  %6.3f || %6.3f  %6.3f  %6.3f || %6.3f" % (nn[i][0], nn[i][1], nn[i][2], nm[i][0], nm[i][1], nm[i][2], math.sqrt(nm[i][0]**2 + nm[i][1]**2 + nm[i][2]**2)), file=Fout) 
  print("\n", file=Fout)

  #calculate angles between each pair of O atoms
  for i in range(5):
    for j in range(i+1,6):
      angle = ((nm[i][0]*nm[j][0] + nm[i][1]*nm[j][1] + nm[i][2]*nm[j][2])
      /math.sqrt(nm[i][0]**2 + nm[i][1]**2 + nm[i][2]**2)
      /math.sqrt(nm[j][0]**2 + nm[j][1]**2 + nm[j][2]**2))
    
      if (abs(angle) > 1.e0):
        angle = 1.*angle/abs(angle)
      angle = np.arccos(angle)/np.arctan(1.)*45.
      print(" angle (degrees) between atoms", i+1 , "and" , j+1 ,"is {0:7.2f}".format(angle))
      print(" angle between", i+1 ,"and", j+1 ,"atom is %7.2f degrees" % (angle), file=Fout)

  kkey = True
  kkey1 = False
  while(kkey == True):
    print(" ")
    print("Would you like to interchange ligands ?")
    print("Yes/No (type Stop to finish)")
    choic = input().split() 
    choic = choic[0]

    if (choic == "Stop"): 
      sys.exit("Done!")
    if (choic == "Yes" or choic == "yes"):
      kkey1 = True

      print("Enter ligands to interchange (e.g. 3 5)")
      inpval = input().split()
      print(inpval)
      par1 = int(inpval[0])-1
      par2 = int(inpval[1])-1

      print(par1+1, "and", par2+1 ,"ligands were interchanged", file=Fout)
      for i in range(3):
        tmp = nm[par2][i]
        nm[par2][i] = nm[par1][i]
        nm[par1][i] = tmp

        tmp = nn[par2][i]
        nn[par2][i] = nn[par1][i]
        nn[par1][i] = tmp

      print(" ")
      print("Check positions:")
      print("\n")
      print("Six nearest", anionname ,"atoms :            ||    these",
            anionname , "became:   ||  distance(Angs):")
      print("(in cartesian coordinates)")
      for i in range(6):
        print(i+1,". coords: {0:6.3f} {1:6.3f} {2:6.3f} || {3:6.3f} {4:6.3f} {5:6.3f} || {6:6.3f}".format(nn[i][0], nn[i][1], nn[i][2], nm[i][0], nm[i][1], nm[i][2], math.sqrt(nm[i][0]**2+nm[i][1]**2 + nm[i][2]**2)))
      print("\n")

      print("Six nearest", anionname ,"atoms :            ||     these",
            anionname , "became:   ||  distance(Angs):", file=Fout)
      print("(in cartesian coordinates)", file=Fout)
      for i in range(6):
        print(i,". coords: %6.3f  %6.3f  %6.3f || %6.3f  %6.3f  %6.3f || %6.3f" % (nn[i][0], nn[i][1], nn[i][2], nm[i][0], nm[i][1], nm[i][2], math.sqrt(nm[i][0]**2 + nm[i][1]**2 + nm[i][2]**2)), file=Fout) 
      print("\n", file=Fout)

    elif (choic == "No" or choic == "no"):
      kkey = False

  #print out distances, if ligands have been exchanged
  if (kkey1 == True):
    print(" ")
    print("Check angles:")
    print("\n")
    for i in range(5):
      for j in range(i+1,6):
        angle = ((nm[i][0]*nm[j][0] + nm[i][1]*nm[j][1] + nm[i][2]*nm[j][2])
        /math.sqrt(nm[i][0]**2 + nm[i][1]**2 + nm[i][2]**2)
        /math.sqrt(nm[j][0]**2 + nm[j][1]**2 + nm[j][2]**2))
        
        if (abs(angle) > 1.e0):
          angle = 1.*angle/abs(angle)
        angle = np.arccos(angle)/np.arctan(1.)*45.
        print(" angle (degrees) between atoms", i+1 , "and" , j+1 ,"is {0:7.2f}".format(angle))
        print(" angle between", i ,"and", j ,"atom is %7.2f degrees" % (angle), file=Fout)

  i0, i1, i2, j1, j2 = input_par()
  iy = 0

  flag = False

  # assume that first 2 atoms will site about new z-direction. Then
  for i in range(i1 - 1, i2):
    angle = ((nm[i0-1][0]*nm[i][0] + nm[i0-1][1]*nm[i][1] + nm[i0-1][2]*nm[i][2])
    /math.sqrt(nm[i0 - 1][0]**2 + nm[i0 - 1][1]**2 + nm[i0 - 1][2]**2)
    /math.sqrt(nm[i][0]**2 + nm[i][1]**2 + nm[i][2]**2))
  
    if ( abs(angle) < 20.0e-2):
      iy = i
      flag = True
    if flag:
      break
  if (flag == True):
    pass
  else:
    print("new x, y axes are not found")
    sys.exit()
  flag = False

  # assume that third atom will site about new x-direction and
  # i-th atom - about new y-direction
  ix = i0-1

  #ix and iy atoms determine the plane with norm vector (a,b,c):
  a = nm[ix][1]*nm[iy][2] - nm[iy][1]*nm[ix][2]
  b = nm[ix][2]*nm[iy][0] - nm[iy][2]*nm[ix][0]
  c = nm[ix][0]*nm[iy][1] - nm[iy][0]*nm[ix][1]
  anorm = math.sqrt(a*a + b*b + c*c)
  a = a/anorm
  b = b/anorm
  c = c/anorm

  # angle between 0p_ix and 0p_iy:
  angle = np.arccos( (nm[ix][0]*nm[iy][0]+nm[ix][1]*nm[iy][1]+nm[ix][2]*nm[iy][2])
  /math.sqrt( nm[ix][0]**2 + nm[ix][1]**2 + nm[ix][2]**2 )
  /math.sqrt( nm[iy][0]**2 + nm[iy][1]**2 + nm[iy][2]**2 ) )

  # difference from 90 degrees devided by two
  vx = np.zeros((2, 3))
  vy = np.zeros((2, 3))
  vz = np.zeros(3)
  wx = np.zeros(3)
  wy = np.zeros(3)
  wz = np.zeros(3)
  ra = np.zeros(3)
  dirc = np.zeros(3)

  angle = (pi/2. - angle)/2.
  if (angle == 0.):
    for i in range(3):
      for j in range(2):
        vx[j][i]=(-1)**j*nm[ix][i]
        vy[j][i]=(-1)**j*nm[iy][i]
    for j in range(2):
      anorm = vx[0][0]*vy[j][0] + vx[0][1]*vy[j][1] + vx[0][2]*vy[j][2]
      if (che(anorm)):
        fx = math.sqrt(vx[0][0]**2 + vx[0][1]**2 + vx[0][2]**2)
        fy = math.sqrt(vy[j][0]**2 + vy[j][1]**2 + vy[j][2]**2)
        fz = math.sqrt(nm[j1-1][0]**2 + nm[j1-1][1]**2 + nm[j1-1][2]**2)
        anorm = (vx[0][0] * (vy[j][1]*nm[j1-1][2] - vy[j][2]*nm[j1-1][1]) 
        - vx[0][1] * (vy[j][0]*nm[j1-1][2] - vy[j][2]*nm[j1-1][0]) 
        + vx[0][2] * (vy[j][0]*nm[j1-1][1] - vy[j][1]*nm[j1-1][0]))
      
        for i in range(3):
          wx[i] = vx[0][i]/fx
          wy[i] = vy[j][i]/fy
          if (anorm > 0):
            wz[i] = nm[j1-1][i]/fz
          elif (anorm < 0):
            wz[i] = nm[j2-1][i]/fz
        flag = True
    if (flag == True):
      pass
    else:
      print(" error in findidng x,y axis in 90 degree case")
      sys.exit()
  else:

  # precise new x
    vx[0][0],vx[0][1],vx[0][2],vx[1][0],vx[1][1],vx[1][2] = rse( a , b , c , 
    nm[ix][0] , nm[ix][1] , nm[ix][2] , 
    np.cos(angle)*math.sqrt(nm[ix][0]**2 + nm[ix][1]**2 + nm[ix][2]**2) ,
    vx[0][0] , vx[0][1] , vx[0][2] , vx[1][0] , vx[1][1] , vx[1][2] )

  # precise new y
    vy[0][0],vy[0][1],vy[0][2],vy[1][0],vy[1][1],vy[1][2] = rse( a , b , c , 
    nm[iy][0] , nm[iy][1] , nm[iy][2] , 
    np.cos(angle)*math.sqrt(nm[iy][0]**2 + nm[iy][1]**2 + nm[iy][2]**2) ,
    vy[0][0] , vy[0][1] , vy[0][2] , vy[1][0] , vy[1][1] , vy[1][2] )

  amatr = np.zeros((3,3))
  bmatr = np.zeros((3,3))
  v = np.zeros((2,3))
  flag1 = False
  flag2 = False
  if (flag == True):
    pass
  else:
    for i in range(2):
      for j in range(2):
        anorm = vx[i][0]*vy[j][0] + vx[i][1]*vy[j][1] + vx[i][2]*vy[j][2]
        if (che(anorm)):
          flag1 = True
          ix = i
          iy = j
          break
      if flag1:
        break
    if (flag1 == True):
      pass
    else:
      print(" error in findidng x,y axis")
      sys.exit()

    anorm = vx[ix][0]*( vy[iy][1]*c - b*vy[iy][2] ) - vx[ix][1]*( vy[iy][0]*c -
     a*vy[iy][2] ) + vx[ix][2]*( vy[iy][0]*b - a*vy[iy][1] )
    vz[0] = anorm * a
    vz[1] = anorm * b
    vz[2] = anorm * c

    # rotation matrix (for vectors !) from initial to rotated basis is:
    amatr[0][0] = vx[ix][0]
    amatr[1][0] = vx[ix][1]
    amatr[2][0] = vx[ix][2]
    amatr[0][1] = vy[iy][0]
    amatr[1][1] = vy[iy][1]
    amatr[2][1] = vy[iy][2]
    amatr[0][2] = vz[0]
    amatr[1][2] = vz[1]
    amatr[2][2] = vz[2]

    

    # coordinates of 1 and 2 vectors in rotated basis:
    v[0][0] , v[0][1] , v[0][2] = rotnb(amatr,nm[j1-1][0],nm[j1-1][1],
                                      nm[j1-1][2],v[0][0],v[0][1],v[0][2])
  
    v[1][0] , v[1][1] , v[1][2] = rotnb(amatr,nm[j2-1][0],nm[j2-1][1],
                                      nm[j2-1][2],v[1][0],v[1][1],v[1][2])
    an1 = np.arccos(v[0][2]/math.sqrt( v[0][0]**2 + v[0][1]**2 + v[0][2]**2))
    an2 = np.arccos(v[1][2]/math.sqrt( v[1][0]**2 + v[1][1]**2 + v[1][2]**2))
    #print(an1,an2)
    iv = 0
    if (an2 < an1):
      iv = 1
    anorm = math.sqrt( v[iv][0]**2 + v[iv][1]**2 + v[iv][2]**2 )
    for i in range(3):
      v[iv][i] = v[iv][i] / anorm

    wz[0],wz[1],wz[2] = rotnb(amatr,vz[0],vz[1],vz[2],wz[0],wz[1],wz[2])
    z = wz[0]*v[iv][0] + wz[1]*v[iv][1] + wz[2]*v[iv][2]

    if (abs(z) > 1.):
      z = 1.*z/abs(z)
    delta = np.arccos(z)

    if (che(delta)):
      for i in range(3):
        wx[i] = vx[ix][i]
        wy[i] = vy[iy][i]
        wz[i] = vz[i]
      flag2 = True
    if (flag2 == True):
      pass
    else:
      ra[0] = wz[1]*v[iv][2] - wz[2]*v[iv][1]
      ra[1] = wz[2]*v[iv][0] - wz[0]*v[iv][2]
      ra[2] = wz[0]*v[iv][1] - wz[1]*v[iv][0]
      anorm = math.sqrt(ra[0]**2 + ra[1]**2 + ra[2]**2)
      for i in range(3):
        ra[i] = ra[i] / anorm

      bmatr = dc2matr(bmatr,delta,ra)
      wx[0],wx[1],wx[2] = rotnb(bmatr,wz[0],wz[1],wz[2],wx[0],wx[1],wx[2])

      if ( not ( che(wx[0] - v[iv][0]) and che(wx[1] - v[iv][1]) 
      and che(wx[2] - v[iv][2]) ) ):
        delta = -delta
        bmatr =  dc2matr(bmatr,delta,ra)
        wx[0],wx[1],wx[2] = rotnb(bmatr,wz[0],wz[1],wz[2],wx[0],wx[1],wx[2])

        if ( not ( che(wx[0] - v[iv][0]) and che(wx[1] - v[iv][1]) 
        and che(wx[2] - v[iv][2]) ) ):
          print("error in finding of additional (delta) rotation")
          sys.exit()
      delta = delta / 2.

      for i in range(3):
        for j in range(3):
          bmatr[j][i] = amatr[i][j]

      dirc[0],dirc[1],dirc[2] = rotnb(bmatr,ra[0],ra[1],ra[2],dirc[0],dirc[1],
                                      dirc[2])

      bmatr =  dc2matr(bmatr,delta,dirc)

      # x,y,z axes in delta-rotated basis are:
      wx[0] , wx[1] , wx[2] = rotnb(bmatr,vx[ix][0],vx[ix][1],vx[ix][2],wx[0],
                                    wx[1],wx[2])
      wy[0] , wy[1] , wy[2] = rotnb(bmatr,vy[iy][0],vy[iy][1],vy[iy][2],wy[0],
                                    wy[1],wy[2])
      wz[0] , wz[1] , wz[2] = rotnb(bmatr,vz[0],vz[1],vz[2],wz[0],wz[1],wz[2])

  # rotation matrix for basis is:
  amatr[0][0] = wx[0]
  amatr[1][0] = wx[1]
  amatr[2][0] = wx[2]
  amatr[0][1] = wy[0]
  amatr[1][1] = wy[1]
  amatr[2][1] = wy[2]
  amatr[0][2] = wz[0]
  amatr[1][2] = wz[1]
  amatr[2][2] = wz[2]

  #Euler angles for the rotation of basis are:
  alpha = 0.
  beta = 0.
  gamma = 0.
  alpha , beta , gamma =  matr2ang(amatr,alpha,beta,gamma)

  #Rotation matrix for vectors:
  #amatr = euler(-alpha,-beta,-gamma,amatr)
  for i in range(3):
    for j in range(3):
      amatr[i][j] = format(amatr[i][j], "9.5f")
  
  print(" ")
  print("Rotation matrix for vectors: \n")
  amatr = amatr.transpose()
  print(amatr)
  print("\n", file=Fout)
  print("Rotation matrix for vectors: \n", file=Fout)
  print(amatr, file=Fout)
  print("\n")

  #print("Positions of ligands in rotated coordinate system" ) 
  #for i in range(0,6):
  # print("Ligand "+str(i+1)+" coords: "+str(round(nm[i][0],4)) + ", "+str(round(nm[i][1],4)) + ", "+str(round(nm[i][2],4)))  


  newposL = np.matmul(nm,amatr)
  print("Positions of ligands in rotated coordinate system" ) 
  for i in range(0,6):
   print("Ligand "+str(i+1)+" coords: "+str(round(newposL[i][0],4)) + ", "+str(round(newposL[i][1],4)) + ", "+str(round(newposL[i][2],4)))  

  lm = 2*2 + 1
  drot = np.zeros((lm, lm))
  drot = mk_rlmc(2,-alpha,-beta,-gamma)
  print(" ")
  print("\n", file=Fout)
  print("Rotation matrix for d-orbitals: \n")
  print("Rotation matrix for d-orbitals: \n", file=Fout)
  print(drot)
  print(drot, file=Fout)
  print("\n", file=Fout)
  print("\n")


  print("Euler angles (radians): {0:10.6f} {1:10.6f} {2:10.6f}".format(-alpha , -beta , -gamma))
  print("the same in pi-units:   {0:10.6f} {1:10.6f} {2:10.6f}".format(-alpha/pi , -beta/pi , -gamma/pi))
  print("         ............................................")
  print("\n", file=Fout)
  print("Euler angles (radians): %10.6f  %10.6f  %10.6f" % (-alpha, -beta, -gamma), file=Fout)
  print("the same in pi-units:: %10.6f  %10.6f  %10.6f" % (-alpha/pi, -beta/pi, -gamma/pi), file=Fout)
  print("         ............................................", file=Fout)

Fout.close()
