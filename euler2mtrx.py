# Developer: S. Streltsov

import numpy as np
import array
import sys
import math

def chop_mtrx(mtrx,prec):
  for i in range(0,len(mtrx)):
    for j in range(0,len(mtrx)):
     if abs(mtrx[i][j])<prec:
       mtrx[i][j] = 0.0
  return mtrx

def mk_rlmc(l,alpha,beta,gamma):

  fa = np.zeros(21)
  for i in range(21):
    fa[i] = math.factorial(i)

  a1 = np.zeros((2*l+1, 2*l+1), 'complex')
  rlm = np.zeros((2*l+1, 2*l+1), 'complex')
  cobeta = np.cos(beta/2.)
  sibeta = np.sin(beta/2.)
  ecom = complex(math.e, 0)
      
  for m in range(-l, l+1):
    for q in range(-l, l+1):
      h_min = max(0, q-m)
      h_max = min(l-m, l+q)
      sum = 0.
      sql = math.sqrt(fa[l+m]*fa[l-m]*fa[l+q]*fa[l-q])
      for h in range(h_min, h_max + 1):
        tmp1 = (fa[l-m-h]*fa[l+q-h]*fa[h-q+m]*fa[h])
        tmp2 = cobeta**(2*l-m+q-2*h)*sibeta**(m-q+2*h)
        sum = sum + (-1)**h*(tmp2*sql/tmp1)
      rlm[m+l, q+l] = ecom**(complex(0., -(q*alpha + m*gamma)))*sum
  
  s = 1./math.sqrt(2.)
  if (l == 0):
    a1[0][0] = complex(1., 0.)
  elif (l == 1):
    a1[0][0] = complex(0., s)
    a1[0][2] = complex(0., s)
    a1[1][1] = complex(1., 0.)
    a1[2][0] = complex(s, 0.)
    a1[2][2] = complex(-s, 0.)
  elif (l == 2):
    a1[0][0] = complex(0., s)
    a1[0][4] = complex(0., -s)
    a1[1][1] = complex(0., s)
    a1[1][3] = complex(0., s)
    a1[2][2] = complex(1., 0.)
    a1[3][1] = complex(s, 0.)
    a1[3][3] = complex(-s, 0.)
    a1[4][0] = complex(s, 0.)
    a1[4][4] = complex(s, 0.)
  else:
    a1[0][0] = complex(0., s)
    a1[0][6] = complex(0., s)
    a1[1][1] = complex(0., s)
    a1[1][5] = complex(0., -s)
    a1[2][2] = complex(0., s)
    a1[2][4] = complex(0., s)
    a1[3][3] = complex(1., 0.)
    a1[4][2] = complex(s, 0.)
    a1[4][4] = complex(-s, 0.)
    a1[5][1] = complex(s, 0.)
    a1[5][5] = complex(s, 0.)
    a1[6][0] = complex(s, 0.)
    a1[6][6] = complex(-s, 0.)

  for lam in range(0, 2*l+1):
    for m in range(0, 2*l+1):
      sumc = complex(0., 0.)
      for nu in range(0, 2*l+1):
        for mu in range(0, 2*l+1):
          sumc = sumc + a1[lam][nu] * rlm[nu][mu] * (a1[m][mu]).conjugate()
      if sumc.imag > 0.001:
         print("Transformation matrix is imaginary. This is wrong! Check!")
         sys.exit();
      drot[lam][m] = sumc.real

  return drot

def euler(a,b,g,t):

  t[0][0] = np.cos(a)*np.cos(b)*np.cos(g) - np.sin(a)*np.sin(g)
  t[0][1] = np.cos(a)*np.cos(b)*np.sin(g) + np.sin(a)*np.cos(g)
  t[0][2] =-np.cos(a)*np.sin(b)

  t[1][0] =-np.sin(a)*np.cos(b)*np.cos(g) - np.cos(a)*np.sin(g)
  t[1][1] =-np.sin(a)*np.cos(b)*np.sin(g) + np.cos(a)*np.cos(g)
  t[1][2] = np.sin(a)*np.sin(b)

  t[2][0] = np.sin(b)*np.cos(g)
  t[2][1] = np.sin(b)*np.sin(g)
  t[2][2] = np.cos(b)

  return t

tmp = input("Do you prefer to use pi-units (pi) or radians (rad): ")
if str(tmp) == 'rad':
    rad = True
    print("Enter alpha, beta, gamma in radians")
elif str(tmp) == 'pi':
    rad = False
    print("Enter alpha, beta, gamma in pi-units")
else:
    sys.exit('Wrong input')

angles = input().split()
if len(angles) != 3:
   print("Something is wrong with angles")
   sys.exit('Wrong input')

alpha = float(angles[0])
beta  = float(angles[1])
gamma = float(angles[2])
pi = 4. * np.arctan(1.)

if rad == False:
   alpha = alpha*pi
   beta  = beta*pi
   gamma = gamma*pi


#this is for d
l = 2
lm = 2*2 + 1

#generate matrix for cubic harmonics
drot = np.zeros((lm, lm))
drot = mk_rlmc(l,-alpha,-beta,-gamma)
drot = chop_mtrx(drot,0.00001)

prot = np.zeros((3, 3))
prot = mk_rlmc(1,-alpha,-beta,-gamma)
prot = chop_mtrx(prot,0.00001)



#generate matrix for rotation in 3D space
rot3D = np.zeros((3,3))
rot3D = euler(alpha,beta,gamma,rot3D)
rot3D = chop_mtrx(rot3D,0.00001)

#save results
Fout = open( "euler2mtrx.out", "w" )
print("Angles: "+str(alpha)+", "+str(beta)+", "+str(gamma)+ "radians", file=Fout)
print("\n", file=Fout)

print("Rotation matrix for d-orbitals (LMTO/VASP order of orbitals): \n", file=Fout)
print("\n")
print("Rotation matrix for d-orbitals (LMTO/VASP order of orbitals):")
print(drot)
print(drot, file=Fout)
print("\n", file=Fout)
print("\n")

print("Rotation matrix for p-orbitals (LMTO/VASP order of orbitals): \n", file=Fout)
print("\n")
print("Rotation matrix for p-orbitals (LMTO/VASP order of orbitals):")
print(prot)
print(prot, file=Fout)
print("\n", file=Fout)
print("\n")


print("Rotation matrix in 3D space\n", file=Fout)
print("Rotation matrix in 3D space")
print(rot3D, file=Fout)
print(rot3D)
Fout.close()
